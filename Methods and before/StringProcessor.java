public class StringProcessor {
    public static final String INPUT_DATA = "Login;Name;Email" + System.lineSeparator() +
            "peterson;Chris Peterson;peterson@outlook.com" + System.lineSeparator() +
            "james;Derek James;james@gmail.com" + System.lineSeparator() +
            "jackson;Walter Jackson;jackson@gmail.com" + System.lineSeparator() +
            "gregory;Mike Gregory;gregory@yahoo.com";

    public static void main(String[] args) {

        String[][] matrix = convertToMatrix(INPUT_DATA);

        System.out.println("===== Convert 1 demo =====");
        convert1(matrix);
        System.out.println();

        System.out.println("===== Convert 2 demo =====");
        convert2(matrix);

    }

    public static String[][] convertToMatrix(String input) {
        String[] words = input.split("\\r?\\n|\\r|;");
        int vert = input.split("\\r?\\n|\\r").length; // = 5
        int hor = 3; // = 3

        String[][] multiWords = new String[vert][hor]; // [5][3]

        for (int i = 0; i < vert; i++) {
            for (int j = 0; j < hor; j++) {
                multiWords[i][j] = words[j + (i * hor)];
            }
        }
        System.out.println(multiWords.length);
        return multiWords;
    }

    public static void convert1(String[][] input) {
        for (int i = 1; i < input.length; i++) {
            System.out.println(input[i][0] + " ==> " + input[i][2]);
        }

    }

    public static void convert2(String[][] input) {
        for (int i = 1; i < input.length; i++) {
            System.out.println(input[i][1] + " (email: " + input[i][2] + ")");
        }
    }

}
