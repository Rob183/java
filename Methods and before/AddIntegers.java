import java.util.Scanner;

public class AddIntegers {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("input 1: ");
        String input1 = sc.next();
        int i1 = Integer.parseInt(input1);
        System.out.println("input 2: ");
        String input2 = sc.next();
        int i2 = Integer.parseInt(input2);
        int sum = i1 + i2;
        System.out.println("summe: " + sum);
        sc.close();
    }

}