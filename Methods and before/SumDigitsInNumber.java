import java.util.Scanner;

public class SumDigitsInNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Please, enter integer: ");
		// int number = sc.nextInt();

		int sumOfDigits = sumDigitsInNumberInt(123456789);
		System.out.println(sumOfDigits);
		sc.close();
	}

	private static int sumDigitsInNumberInt(int i) {
		int sum = 0;

		while (i != 0) {
			sum = sum + (i % 10);
			i = i / 10;
		}
		return Math.abs(sum);
	}

	public static int sumDigitsInNumber(int number) {
		String stringNumber = Integer.toString(number);
		int sum = 0;

		char[] digits = stringNumber.toCharArray();
		for (int i = 0; i < digits.length; i++) {
			sum = sum + Character.getNumericValue(digits[i]);
		}
		return sum;
	}
}