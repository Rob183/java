import java.util.Scanner;
import java.lang.Math;
import java.math.RoundingMode;
import java.math.BigDecimal;

public class PrintTriangle {

    public static void main(String[] args) {
        // calculateTriangle();
        circleCircumference();
    }

    static void circleCircumference() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please, enter circle radius: ");

        double radius = sc.nextDouble();
        double circumference = 2 * radius * Math.PI;
        BigDecimal bigCircumference = BigDecimal.valueOf(circumference).setScale(2, RoundingMode.HALF_UP);

        System.out.println("Circle circumference is: " + bigCircumference);
        sc.close();
    }

    static void calculateTriangle() {
        Scanner sc = new Scanner(System.in);
        try {
            System.out.print("Please, enter A side of a triangle: ");
            double a = sc.nextDouble();

            System.out.print("Please, enter B side of a triangle: ");
            double b = sc.nextDouble();

            System.out.print("Please, enter C side of a triangle: ");
            double c = sc.nextDouble();

            double p = (a + b + c) / 2;
            double area = Math.sqrt(p * (p - a) * (p - b) * (p - c));

            BigDecimal bigArea = BigDecimal.valueOf(area).setScale(2, RoundingMode.HALF_UP);
            System.out.println("Triangle area is: " + bigArea);

        } catch (Exception e) {
            System.out.println("NaN: " + e);
        }
        sc.close();
    }
}
