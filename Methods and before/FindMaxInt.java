import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class FindMaxInt {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please, enter integer numbers separated by space: ");

        String numbers = sc.nextLine();

        int[] intArray = convertStringArrayToIntArray(numbers.split(" "));
        System.out.println("*** Initial Array ***");
        System.out.println(Arrays.toString(intArray));

        System.out.println("*** Max number in array ***");
        System.out.println(findMaxIntInArray(intArray));

        sc.close();
    }

    private static int[] convertStringArrayToIntArray(String[] stringArray) {
        int[] intArray = new int[stringArray.length];
        for (int index = 0; index < stringArray.length; index++) {
            intArray[index] = Integer.valueOf(stringArray[index]);
        }
        return intArray;
    }

    public static int findMaxIntInArray(int[] intArray) {
        int maxValue = 0;
        for (int i = 0; i < intArray.length; i++) {
            if (maxValue < intArray[i]) {
                maxValue = intArray[i];
            }
        }
        return maxValue;
    }
}
