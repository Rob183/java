import java.util.Random;
import java.util.Scanner;
import java.lang.String;

public class IfELse {

	public static void main(String[] args) {
		Random rn = new Random();
		int test = rn.nextInt(5) + 5;
		System.out.println(test);
		login(args);

	}

	private static void login(String[] args) {
		// !! first try
		Scanner sc = new Scanner(System.in);
		System.out.println("Please input mode");
		String mode = sc.nextLine();
		System.out.println(mode);
		if (mode.equals("--admin")) {
			System.out.print("Hello,Admin");
		} else if (mode == "--guest") {
			System.out.print("Hello,Guest");
		} else {
			System.out.print("Please, select either 'ADMIN' or 'GUEST' mode for this program");
		}

		// !!second try
		String inputArguments = String.join(",", args);
		if (inputArguments.contains("--admin") && inputArguments.contains("--guest")) {
			System.out.println("Please select either one");
		} else if (inputArguments.contains("--admin")) {
			System.out.println("all good in tha hood");
		} else {
			System.out.println("no");
		}
		sc.close();
	}
}

// Program is started with two arguments (numbers)

// Program adds these two numbers

// In case one of the input arguments is floating-point number - the result of
// addition is floating-point number

// In case two arguments are integer - the result of addition is integer