import java.util.Scanner;

public class NewLineForDigit {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();

        for (char digit : a.toCharArray()) {
            System.out.println(digit);
        }
        sc.close();
        
    }
}
