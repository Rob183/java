import java.util.Scanner;

public class GreatestCommonDivisor {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please, enter two numbers separated by space: " + args[0] + " " + args[1]);
		// String userInput = sc.nextLine();

		// String[] stringArray = userInput.split(" ");
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		// System.out.println("test: " + 12%27);
		System.out.println(gcdRecursive(b, a));
		sc.close();
	}

	public static int gcdRecursive(int firstNumber, int secondNumber) {

		if (secondNumber == 0) {
			return Math.abs(firstNumber);
		} else {
			return gcdRecursive(secondNumber, firstNumber%secondNumber);
		}
	}
}