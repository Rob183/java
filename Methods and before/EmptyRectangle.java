import java.util.Scanner;

public class EmptyRectangle {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please, enter height of rectangle: ");
        int height = sc.nextInt();
        System.out.print("Please, enter width of rectangle: ");
        int width = sc.nextInt();

        drawRectangle(height, width);
        sc.close();
    }

    public static void drawRectangle(int height, int width) {

        String stringHeight = "*";
        String stringWidth = "";

        for (int i = 0; i < width; i++) {
            stringWidth = stringWidth + "*";
            stringHeight = stringHeight + " ";
        }

        stringHeight = stringHeight + "*";
        System.out.println("stringwidth: " + stringWidth);
        System.out.println("stringheight: " + stringHeight);

    }

}
