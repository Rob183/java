import java.util.Scanner;

public class SimpleNumberCalculator {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Bitte a eintippen");
        Double a = sc.nextDouble();

        System.out.println("Bitte b eintippen");
        Double b = sc.nextDouble();

        Double sum = a + b;

        if (sum == Math.ceil(sum)) {
            int IntSum = sum.intValue();
            System.out.println(("Int: " + IntSum));
        } else {
            float FloatSum = sum.floatValue();
            System.out.println("float: " + FloatSum);
        }
        sc.close();
    }
}