import java.util.Arrays;
import java.util.Scanner;

public class FirstCharCapital {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please, enter any text: ");
        // String userInput = sc.nextLine();
        String userInput = "Hallo mein Name ist Robert und ich komme aus dem schönen Zella_mehlis.";

        System.out.println(firstCharToTitleCase(userInput));
        sc.close();
    }

    public static String firstCharToTitleCase(String text) {
        text = text.toLowerCase();
        String[] textArray = text.split(" ");
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < textArray.length; i++) {
            String temp = textArray[i].substring(0, 1).toUpperCase();
            sb.append(temp + textArray[i].substring(1) + " ");
        }

        String joinedString = sb.toString();

        return joinedString;
    }
}