package Enums;

import java.util.Scanner;
import java.util.stream.Stream;

public class DemoEnum {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("Please, enter message type to check it priority: ");
			String userInput = sc.next();

			if (isValidMessageType(userInput)) {

				Priority prio = Priority.valueOf(userInput.toUpperCase());

				System.out.println(prio.getPrio());
				break;
			} else {
				System.out.println("Please, enter valid "
						+ "message type. Only 'A', 'B', 'C' or 'D' are allowed");
				continue;
			}

		}

	}

	private static boolean isValidMessageType(String userInput) {
		if (Stream.of("A", "B", "C", "D").anyMatch(userInput::equalsIgnoreCase)) {
			return true;
		} else {
			return false;
		}
	}

}
