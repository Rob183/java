package Enums;

public enum Priority {

    A("HIGH"), B("Medium"), C("LOW"), D("LOW");

    private String prio;

    private Priority(String prio) {
        this.prio = prio;
    }

    public String getPrio() {
        return this.prio;
    }
}

// Implement console program which will meet the following requirements:

// Program starts and ask user to enter message type

// Only ‘A’, ‘B’, ‘C’ or ‘D’ message types are allowed.

// In case user entered invalid message type, program asks to enter message type
// again

// When valid message type is entered program prints message type priority to
// console

// Priority and Message Type implemented as enum types

// Message types have next priorities:

// Message Type Priority

// A HIGH

// B MEDIUM

// C LOW

// D LOW

// MessageType enum has next method:

// public Priority getPriority() {

// <write your code here>

// }